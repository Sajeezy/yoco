//
//  BusinessCategories.swift
//  yoco
//
//  Created by Steven Sajja on 19/11/2015.
//  Copyright © 2015 stevensajja. All rights reserved.
//

import Foundation

class BusinessCategories {
    var categoryId: String = ""
    var categoryName: String = ""
    var categoryGroup: String = ""
}