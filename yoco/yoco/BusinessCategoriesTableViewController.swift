//
//  BusinessCategoriesTableViewController.swift
//  yoco
//
//  Created by Steven Sajja on 19/11/2015.
//  Copyright © 2015 stevensajja. All rights reserved.
//

import UIKit

class BusinessCategoriesTableViewController: UITableViewController, UISearchBarDelegate, UISearchDisplayDelegate  {
   
    let yocoURLString = "http://yoco-core-staging.herokuapp.com/api/common/v1/properties/businessCategories"
    var businessCategories = [BusinessCategories]()
   
    var filteredBusinessCategories = [BusinessCategories]()
    
    var businessIndexTitles = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
        "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getBusinessCategories() // fetched from our JSON REST API
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return self.filteredBusinessCategories.count
        } else {
            return self.businessCategories.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier = "Cell"
        let cell = self.tableView.dequeueReusableCellWithIdentifier(cellIdentifier)! as UITableViewCell
        
        var bCategory: BusinessCategories
        if tableView == self.searchDisplayController!.searchResultsTableView {
            bCategory = filteredBusinessCategories[indexPath.row]
        } else {
            bCategory = businessCategories[indexPath.row]
        }
        
        cell.textLabel?.text = businessCategories[indexPath.row].categoryName
        cell.detailTextLabel?.text = businessCategories[indexPath.row].categoryGroup
        
        return cell
    }
    
    override func tableView(tableView: UITableView, sectionForSectionIndexTitle
        title: String, atIndex index: Int) -> Int {
            guard let index = businessIndexTitles.indexOf(title) else {
                return -1
            }
            return index
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return businessIndexTitles[section]
    }

    override func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
        return businessIndexTitles
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection
        section: Int) -> CGFloat {
            return 50
    }

    override func tableView(tableView: UITableView, willDisplayHeaderView view:
        UIView, forSection section: Int) {
            let headerView = view as! UITableViewHeaderFooterView
            headerView.textLabel?.textColor = UIColor.orangeColor()
            headerView.textLabel?.font = UIFont(name: "Avenir", size: 25.0)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
//        When a user selects a category it should pop up an alert thanking them and displaying the category that they chose.
       
        var category = "The name is: \(businessCategories[indexPath.row].categoryName), the category group is: \(businessCategories[indexPath.row].categoryGroup) & the ID is: \(businessCategories[indexPath.row].categoryId)"
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Default, handler: nil)
        
        let alertController = UIAlertController(title: "Yoco \(indexPath.row)",message: category,preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    func getBusinessCategories() {
        
        let url = NSURL(string: yocoURLString)
        
        if url != nil {
            let request = NSMutableURLRequest(URL: url!)
            request.HTTPMethod = "GET"
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {
                (data, response, error) -> Void in
                
                if let error = error {
                    print(error)
                    return
                }
                
                //Fetch data
                if let data = data {
                    self.businessCategories = self.fetchDataFromUrl(data)
                    
                    //reload table
                    NSOperationQueue.mainQueue().addOperationWithBlock({() -> Void in
                        self.tableView.reloadData()
                    })
                }
                
            })
            task.resume()
        }
    }
    
    
    func fetchDataFromUrl(data: NSData) -> [BusinessCategories] {
        do {
            let jsonResult = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
            
            //PARSE JSON DATA
            let jsonCategories = jsonResult?["data"]as! [AnyObject]
            
            for category in jsonCategories {
                print("Successfully retrieved \(category.count) "+" \(jsonCategories.count)" )
                let businessCat = BusinessCategories()
                businessCat.categoryId = category[0] as! String
                businessCat.categoryName = category[1] as! String
                businessCat.categoryGroup = category[2] as! String
                businessCategories.append(businessCat)

            }
        } catch {
            print(error)
        }
       
        return businessCategories
        
    }

    
    func filterContent(searchText: String, scope: String = "All") {
        self.filteredBusinessCategories = self.businessCategories.filter({( aCategory : BusinessCategories) -> Bool in
            var categoryMatch = (scope == "All") || (aCategory.categoryName == scope)
            var stringMatch = aCategory.categoryName.rangeOfString(searchText)
            return categoryMatch && (stringMatch != nil)
        })
    }
   
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
        let scopes = self.searchDisplayController!.searchBar.scopeButtonTitles! as [String]
        let selectedScope = scopes[self.searchDisplayController!.searchBar.selectedScopeButtonIndex] as String
        self.filterContent(searchString, scope: selectedScope)
        return true

    }
    
    
    func searchDisplayController(controller: UISearchDisplayController!,shouldReloadTableForSearchScope searchOption: Int) -> Bool {
            let scope = self.searchDisplayController!.searchBar.scopeButtonTitles! as [String]
            self.filterContent(self.searchDisplayController!.searchBar.text!, scope: scope[searchOption])
            return true
    }

}
